
function request(url) {
  return new Promise((resolve) => {
    let ajax = new XMLHttpRequest();

    ajax.onload = function () {
      if (ajax.status === 200) {
        try {
          return resolve(JSON.parse(ajax.response));
        } catch (e) {
          return resolve([]);
        }
      }
      return resolve([]);
    };

    ajax.onerror = function () {
      return resolve([]);
    };

    ajax.open('GET', url);
    ajax.send();
  });
}

export default {
  request,
};
