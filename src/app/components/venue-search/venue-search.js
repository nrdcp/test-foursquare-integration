import appConfig from '../../config/config';

import venueSearchService from './venue-search.service';

class VenueSearch {
  constructor() {
    this.classNamePfx = 'venue-search';
    this.searchLocation = 'London, UK';
    this.lastSearchQuery = null;

    this.vm = {
      form: null,
      input: null,
      results: null,
    };

    this.initTemplate();
    this.initListeners();
  }

  initListeners() {
    this.vm.form.addEventListener('submit', this.onFormSubmitted.bind(this), false);
  }

  initTemplate() {
    /* obviously proper templating tools should be used ideally */

    // form elements
    let formInputEl = document.createElement('input');
    formInputEl.type = 'text';
    formInputEl.name = 'keyword';
    formInputEl.value = '';
    formInputEl.placeholder = 'e.g. Museum, Hotel, Cheese Shop';

    let formLabelEl = document.createElement('label');
    formLabelEl.htmlFor = 'keyword';
    formLabelEl.appendChild(formInputEl);

    let formButtonEl = document.createElement('button');
    formButtonEl.type = 'submit';
    formButtonEl.innerHTML = 'Search';

    let formEl = document.createElement('form');
    formEl.setAttribute('novalidate', 'novalidate');
    formEl.appendChild(formLabelEl);
    formEl.appendChild(formButtonEl);

    // header
    let headerElSpan = document.createElement('span');
    headerElSpan.innerHTML = this.searchLocation;

    let headerEl = document.createElement('h3');
    headerEl.innerHTML = 'Foursquare top venues search in ';
    headerEl.appendChild(headerElSpan);

    // resulst container
    let resultsContainerEl = document.createElement('div');
    resultsContainerEl.className = `${this.classNamePfx}__results`;

    // container
    let containerEl = document.createElement(`${this.classNamePfx}`);
    containerEl.className = `${this.classNamePfx}`;
    containerEl.appendChild(headerEl);
    containerEl.appendChild(formEl);
    containerEl.appendChild(resultsContainerEl);

    // container wrapper
    let wrapperEl = document.querySelector(appConfig.APP_WRAPPER_SELECTOR);
    wrapperEl.appendChild(containerEl);

    this.vm.form = document.querySelector(`${this.classNamePfx} form`);
    this.vm.input = document.querySelector(`${this.classNamePfx} form input`);
    this.vm.results = document.querySelector(`${this.classNamePfx} .${this.classNamePfx}__results`);

    this.vm.input.focus();
  }

  wrapSearchResults(venues) {
    let listEl = document.createElement('ul');

    venues.forEach((venue, index) => {
      let listItemSpanEl = document.createElement('span');
      listItemSpanEl.innerHTML = `${index + 1}.`;

      let listItemLinkEl = document.createElement('a');
      listItemLinkEl.href = venue.url;
      listItemLinkEl.innerHTML += venue.name;

      let listItemEl = document.createElement('li');
      listItemEl.appendChild(listItemSpanEl);
      listItemEl.appendChild(listItemLinkEl);

      listEl.appendChild(listItemEl);
    });

    return listEl;
  }

  wrapNoResultsMessage() {
    let el = document.createElement('p');
    el.innerHTML = 'Nothing found, sadly. Try looking for something else.';

    return el;
  }

  setValidationError() {
    this.vm.form.className = 'is-invalid';
  }

  removeValidationError() {
    this.vm.form.className = '';
  }

  hasValidationError() {
    return this.vm.form.className === 'is-invalid';
  }

  onFormSubmitted(e) {
    e.preventDefault();

    let searchQuery = this.vm.input.value;

    if (!searchQuery) {
      this.setValidationError();
      return;
    }

    if (searchQuery === this.lastSearchQuery) {
      return;
    }

    if (this.hasValidationError()) {
      this.removeValidationError();
    }

    venueSearchService.search(searchQuery, this.searchLocation)
      .then((venues) => {
        this.vm.results.innerHTML = '';

        if (venues.length) {
          this.vm.results.appendChild(this.wrapSearchResults(venues));
        } else {
          this.vm.results.appendChild(this.wrapNoResultsMessage());
        }

        this.lastSearchQuery = searchQuery;
      });
  }
}

export default VenueSearch;
