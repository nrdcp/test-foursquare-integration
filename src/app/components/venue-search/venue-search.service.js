
import appAuthConfig from '../../config/auth-config';
import utils from '../../utils/utils';

function getSearchUrl(query, location) {
  return `https://api.foursquare.com/v2/venues/search?near=${location}&query=${query}&limit=10&intent=browse&client_id=${appAuthConfig.API_CLIENT_ID}&client_secret=${appAuthConfig.API_CLIENT_SECRET}&v=20161029`;
}

function transformSearchResponse(venues) {
  let transformed = [];

  venues.map((venue) => {
    let data = {
      name: venue.name,
      url: `https://foursquare.com/v/${venue.id}`,
    };

    return transformed.push(data);
  });

  return transformed;
}

function search(query, location) {
  return utils.request(getSearchUrl(query, location))
    .then((data) => {
      if (data && data.response && data.response.venues) {
        return Promise.resolve(transformSearchResponse(data.response.venues));
      }

      return Promise.resolve([]);
    })
    .catch(() => Promise.resolve([]));
}

export default {
  search,
};
