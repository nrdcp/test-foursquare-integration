
import './app.scss';

import VenueSearch from './components/venue-search/venue-search';

document.addEventListener('DOMContentLoaded', () => {
  new VenueSearch();
});

export default {};
