# Foursquare API Integration Test

A sample webapp consisting of a search form returning top Foursquare venues based on user's query.

* Written entirely in ES6
* ESlint-compliant code
* Components-based structure
* Sass for styles
* Gulp + Webpack for tooling

## Demo
Visit [http://www.nrdcp.net/test-foursquare-integration](http://www.nrdcp.net/test-foursquare-integration) to see a live project demo.

## Getting Started
### Dependencies
Tools needed to run this app:
* `node` (tested on v6.3.1) and `npm`

### Installing
* `fork` this repo
* `clone` your fork
* `npm install -g gulp karma karma-cli webpack` install global cli dependencies
* `npm install` to install dependencies
* Create a copy of `src/app/config/auth-config.sample.js`, rename to `auth-config.js` and populate with your generated app credentials from [Foursquare](https://developer.foursquare.com/start)

#### Gulp Tasks
A list of available tasks:
* `serve`
  * starts a dev server via `webpack-dev-server`, serving the `src` folder.
* `build`
  * runs Webpack, which will transpile, concatenate, and compress (collectively, 'bundle') all assets and code into `dist/bundle.js`. It also prepares `index.html` to be used as application entry point, links assets and created dist version of our application.
